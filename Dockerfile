ARG BASE
FROM $BASE

USER root
ARG VECTORS_VERSION \
    VECTORS_ARCH
# Env PG_MAJOR is present in cnpg images
RUN if [ -z "${VECTORS_ARCH:-}" ]; then \
        VECTORS_ARCH="$(dpkg --print-architecture)"; \
    fi \
    && if ! [ "$VECTORS_ARCH" = "amd64" ] && ! [ "$VECTORS_ARCH" = "arm64" ]; then \
        >&2 echo "Unsupported architecture" && exit 1; \
    fi \
    && apt update && apt install -y wget \
    && wget -O vectors.deb https://github.com/tensorchord/pgvecto.rs/releases/download/v${VECTORS_VERSION}/vectors-pg${PG_MAJOR}_${VECTORS_VERSION}_${VECTORS_ARCH}.deb \
    && dpkg -i vectors.deb \
    && rm vectors.deb \
    && apt remove -y wget
USER 26